import 'package:flutter/material.dart';

class Thongtinlienlac extends StatefulWidget {
  const Thongtinlienlac({ Key? key }) : super(key: key);

  @override
  State<Thongtinlienlac> createState() => _ThongtinlienlacState();
}

class _ThongtinlienlacState extends State<Thongtinlienlac> {
    List<String> listsearch = [
    'Phạm Minh Đức',
    'Đao Ngọc Hân',
    'Thảo Nguyên',
    'Võ Ngọc Quỳnh Anh',
    'Dương Hồng Phương',
    'Mai Lê Tiến Đạt',
    'Đoàn Văn Nghĩa',
    'Đàm Việt Cường',
    'Trần Hoài Phát',
    'Hoàng Trung Đức',
    'Thích Cảnh Long',
    'Vũ Hồng Ngọc',
    'Vũ Văn Ngọc',
    'Trần Thiên Phát',
    'Hồ Ngọc Thiện',
    'Lê Ngọc Thạch',
    'Trần Vĩnh Kha',
    'Phạm Thái Kỳ Trung',
    'Ngô Sĩ Liên',
    'Nguyễn Quang Tiến',
    'Bùi Khang Thịnh',
    'Tống Lăm Tường',
    'Phan Trần Hà Phương',
    'Ngô Phụng',
    'Mai Văn Mạnh',
  ];
  List<String>? ListSearch;
  TextEditingController Name = TextEditingController();
  @override
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.blue.shade300,
            title: Container(
              decoration: BoxDecoration(
                  color: Colors.blue.shade200,
                  borderRadius: BorderRadius.circular(20)),
              child: TextField(
                controller: Name,

                cursorColor: Colors.black,
                decoration: InputDecoration(
                    hintText: 'Nhập tên tìm kiếm',
                    contentPadding: EdgeInsets.all(8)),
              ),
            )),
        body: Name.text.isNotEmpty &&
                ListSearch!.length == 0
            ? Center(

              )
            : ListView.builder(
                itemCount: Name.text.isNotEmpty
                    ? ListSearch!.length
                    : listsearch.length,
                itemBuilder: (ctx, index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        CircleAvatar(
                          child: Icon(Icons.person),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(Name.text.isNotEmpty
                            ? ListSearch![index]
                            : listsearch[index]),
                      ],
                    ),
                  );
                }));
  }
}