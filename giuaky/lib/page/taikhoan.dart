import 'package:flutter/material.dart';

class taikhoan extends StatefulWidget {
  const taikhoan(
      {Key? key, required void Function(dynamic index) changeIndexedStack})
      : super(key: key);

  @override
  State<taikhoan> createState() => _taikhoanState();
}

class _taikhoanState extends State<taikhoan> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Đăng Nhập",
      home: Scaffold(
        body: LoginScreen(),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(20.0),
        child: Form(
            key: formKey,
            child: Column(
              children: [
                emailField(),
                passwordField(),
                loginButton(),
              ],
            )));
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(labelText: "Email"),
      onSaved: (value) {
        email = value as String;
      },
    );
  }

  Widget passwordField() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(labelText: "Mật Khẩu"),
      onSaved: (value) {
        print('onSaved: value=$value');
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();
          }
        },
        child: Text('Đăng Nhập'));
  }
}
