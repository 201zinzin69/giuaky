import 'package:flutter/material.dart';
import 'package:giuaky/page/add%20to%20card/giohang.dart';



class sanpham extends StatefulWidget {
  const sanpham({ Key? key }) : super(key: key);

  @override
  State<sanpham> createState() => _sanphamState();
}

class _sanphamState extends State<sanpham> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sản Phẩm'),
        actions:[

          IconButton(
            onPressed:(){
              Navigator.push(context,MaterialPageRoute(builder:(context) => giohang()));
            },
            icon: const Icon(Icons.shopping_bag)
          ),
        ]
      ),

      body: Center(
        child: Column(
          children: [
            Flexible(
              flex: 1,
              
              child: Text(
                'TỔNG SẢN PHẨM',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                  color: Colors.green
                )
              ),
            ),
            Flexible(
              flex: 12,
              child: GridView.extent(
              primary: false,  
              padding: const EdgeInsets.all(16),  
              crossAxisSpacing: 100,  
              mainAxisSpacing: 10,  
              maxCrossAxisExtent: 200.0,  
              children: <Widget>[
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('quan4.jpg')),  
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('quan1.jpg')),  
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('quan2.jpg')),    
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('quan3.jpg')),    
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('quan5.jpg')),  
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('quan6.jpg')),    
              ), 
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('quan7.png')),    
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('quan8.jpg')),    
              ),
                            Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('ao4.png')),  
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('ao1.jpg')),  
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('ao2.jpg')),    
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('ao3.jpg')),    
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('ao5.jpg')),  
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('ao6.jpg')),    
              ), 
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('ao7.jpg')),    
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('ao8.jpg')),    
              ),
               Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('vest4.jpg')),  
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('vest1.jpg')),  
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('vest2.jpg')),    
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('vest3.jpg')),    
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('vest5.jpg')),  
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('vest4.jpg')),    
              ), 
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('vest7.jpg')),    
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('vest8.jpg')),    
              ),  
              ]
              ),
            )
          ],
        ),
      ) ,
    );
  }
}

