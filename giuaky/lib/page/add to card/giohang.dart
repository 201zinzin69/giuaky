import 'dart:html';

import 'package:flutter/material.dart';

class giohang extends StatefulWidget {
  const giohang({ Key? key }) : super(key: key);

  @override
  State<giohang> createState() => _giohangState();
}

class _giohangState extends State<giohang> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Gio Hang') ,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal :16.0),
        child: Column(
        children: <Widget>[
          Text(
            "Giỏ Hàng",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 21.0,
            ),
            ),
            SizedBox(height: 12.0,),
            Row(
              children: <Widget>[
              Container(
                width: 80.0,
                height: 80.0,
                child: Center(
                  child: Container(
                    decoration: BoxDecoration(
                    color: Colors.red[100],
                      image: DecorationImage(
                      image: AssetImage('quan4.jpg')
                      ),
                      borderRadius: BorderRadius.circular(20.0)
                      ),
                    )
                  ),
                ),
                SizedBox(width: 12.0),
                Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: 100.0,
                      child: Text("Quần dài xám", 
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    Row(
                      children: <Widget>[
                        Container(
                          width: 20.0,
                          height: 20.0,
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius: BorderRadius.circular(4.0)
                          ),
                          child: Icon(Icons.add,
                          color: Colors.white,
                          size:15.0, 
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal:8.0),
                          child: Text(
                          '1',
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        
                      ),
                        Container(
                          width: 20.0,
                          height: 20.0,
                          decoration: BoxDecoration(
                            color: Colors.blue[300],
                            borderRadius: BorderRadius.circular(4.0)
                          ),
                          child: Icon(Icons.add,
                          color: Colors.white,
                          size:15.0, 
                          ),
                        ),
                        Spacer(),
                        Text(
                          '120,000đ',
                          style: TextStyle(
                            
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
              Row(
              children: <Widget>[
              Container(
                width: 80.0,
                height: 80.0,
                child: Center(
                  child: Container(
                    decoration: BoxDecoration(
                    color: Colors.red[100],
                      image: DecorationImage(
                      image: AssetImage('ao5.jpg')
                      ),
                      borderRadius: BorderRadius.circular(20.0)
                      ),
                    )
                  ),
                ),
                SizedBox(width: 12.0),
                Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: 100.0,
                      child: Text("Áo thun oversize trắng", 
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    Row(
                      children: <Widget>[
                        Container(
                          width: 20.0,
                          height: 20.0,
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius: BorderRadius.circular(4.0)
                          ),
                          child: Icon(Icons.add,
                          color: Colors.white,
                          size:15.0, 
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal:8.0),
                          child: Text(
                          '1',
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        
                      ),
                        Container(
                          width: 20.0,
                          height: 20.0,
                          decoration: BoxDecoration(
                            color: Colors.blue[300],
                            borderRadius: BorderRadius.circular(4.0)
                          ),
                          child: Icon(Icons.add,
                          color: Colors.white,
                          size:15.0, 
                          ),
                        ),
                        Spacer(),
                        Text(
                          '150,000đ',
                          style: TextStyle(
                            
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
              Row(
              children: <Widget>[
              Container(
                width: 80.0,
                height: 80.0,
                child: Center(
                  child: Container(
                    decoration: BoxDecoration(
                    color: Colors.red[100],
                      image: DecorationImage(
                      image: AssetImage('ao4.jpg')
                      ),
                      borderRadius: BorderRadius.circular(20.0)
                      ),
                    )
                  ),
                ),
                SizedBox(width: 12.0),
                Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: 100.0,
                      child: Text("Áo tidie", 
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(height: 8.0),
                    Row(
                      children: <Widget>[
                        Container(
                          width: 20.0,
                          height: 20.0,
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius: BorderRadius.circular(4.0)
                          ),
                          child: Icon(Icons.add,
                          color: Colors.white,
                          size:15.0, 
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal:8.0),
                          child: Text(
                          '1',
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        
                      ),
                        Container(
                          width: 20.0,
                          height: 20.0,
                          decoration: BoxDecoration(
                            color: Colors.blue[300],
                            borderRadius: BorderRadius.circular(4.0)
                          ),
                          child: Icon(Icons.add,
                          color: Colors.white,
                          size:15.0, 
                          ),
                        ),
                        Spacer(),
                        Text(
                          '240,000đ',
                          style: TextStyle(
                            
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
              Container(
                width: 5.0,
                height: 80.0,

                ),
                SizedBox(width: 20.0),
                Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: 100.0,
                      child: Text("Tổng giá: 510,000đ", 
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(height: 50.0),
                      Container(
                        width: 200.0,
                        child: MaterialButton(
                          onPressed: (){},
                          color: Colors.orange,
                          child: Text(
                            'TIẾN HÀNH ĐẶT HÀNG',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 14.0
                            ),)
                        )
                      )
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}