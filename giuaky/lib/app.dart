import 'package:flutter/material.dart';

import 'package:giuaky/page/Thongbao.dart';
import 'package:giuaky/page/Thongtinlienlac.dart';
import 'package:giuaky/page/cuahang.dart';
import 'package:giuaky/page/sanpham.dart';
import 'package:giuaky/page/taikhoan.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: AppHomePage(),
    );
  }
}

class AppHomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppHomePageState();
  }
}

class AppHomePageState extends State<AppHomePage> {
  int currentIndex = 0;
  void changeIndexedStack(index) {
    setState(() {
      currentIndex = index;
    });
  }

  late List<Widget> screens;
  @override
  void initState() {
    screens = [
      cuahang(),
      sanpham(),
      taikhoan(changeIndexedStack: changeIndexedStack),
      Thongbao(),
      Thongtinlienlac()
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        //-> switch giua cac trang
        index: currentIndex,
        children: screens,
      ),
      bottomNavigationBar: BottomNavigationBar(
          //=> bottom navigation bar
          type: BottomNavigationBarType.fixed,
          iconSize: 35,
          backgroundColor: Colors.white,
          selectedItemColor: Colors.orange,
          onTap: (index) {
            setState(() {
              currentIndex = index;
            });
          },
          currentIndex: currentIndex,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: "Cửa hàng",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.remove_red_eye),
              label: "Tổng sản phẩm",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.login),
              label: "Đăng Nhập",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.notification_add),
              label: "Thông báo",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: "Thông tin liên lạc",
            ),
          ]),
    );
  }
}
